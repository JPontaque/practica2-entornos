package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		/*
		 * El error sucede cuando busca la posicion del caracter,
		 * debido a que no encuentra ese caracter, y peta el programa.
		 * 
		 * Se puede solucionar buscando un caracter que si pueda estar
		 * en la cadena de texto que ponemos.
		 */
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
