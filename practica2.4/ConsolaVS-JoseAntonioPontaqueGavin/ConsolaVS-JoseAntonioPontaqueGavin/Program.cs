﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS_JoseAntonioPontaqueGavin
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Introduce una opcion del menu: ");
            Console.WriteLine("1 - Sumar longitud de cadenas de texto.");
            Console.WriteLine("2 - Escribir una cadena de texto al reves.");
            Console.WriteLine("3 - Factorial de un numero.");
            Console.WriteLine("4 - Salir");

            int numero;

            Console.WriteLine("Introduce opcion");
            numero = int.Parse(Console.ReadLine());

            switch (numero)
            {
                case 1:

                    sumarCadenas();

                    break;

                case 2:

                    cadenaAlReves();

                    break;

                case 3:

                    numeroFactorial();

                    break;

                case 4:

                    salirMenu();

                    break;

                default:

                    opcionNoValidaMenu();

                    break;
                    
            }

        }

        private static void numeroFactorial()
        {

            int numero;
            int resultado = 1;

            Console.WriteLine("Introduce un numero");
            numero = int.Parse(Console.ReadLine());

            if(numero > 0)
            { 

                for(int i = 1; i <= numero; i++ )
                {

                    resultado = resultado * i;

                }

                Console.WriteLine("El factorial de tu numero '" + numero + "' es = " + resultado);
                Console.ReadKey();

            }

            else

            {

                Console.WriteLine("El factorial de tu numero '" + numero +"' es = 0");
                Console.ReadKey();

            }

        }

        private static void opcionNoValidaMenu()
        {

            Console.WriteLine("Has usado una opcion no contemplada, hasta la proxima.");
            Console.ReadKey();

        }

        private static void salirMenu()
        {

            Console.WriteLine("Has usado la opcion salir, hasta la proxima.");
            Console.ReadKey();

        }

        private static void cadenaAlReves()
        {
            String cadena;
            String cadenaReves = "";

            Console.WriteLine("Escribe una cadena de texto");
            cadena = Console.ReadLine();

            for (int i = 0; i < cadena.Length; i++)
            {

                cadenaReves = cadena.ElementAt(i) + cadenaReves;

            }

            Console.WriteLine("Cadena al reves: " + cadenaReves);
            Console.ReadKey();

        }

        private static void sumarCadenas()
        {

            int numero;

            do
            {
                Console.WriteLine("Introduce numero de cadenas a sumar, no puede ser 0");
                numero = int.Parse(Console.ReadLine());

            } while (numero == 0);



            String cadenas;
            int resultado = 0;

            for (int i = 0; i < numero; i++)
            {
                Console.WriteLine("Introduce cadena de texto");
                cadenas = Console.ReadLine();
                resultado = resultado + cadenas.Length;

            }

            Console.WriteLine("Longitud: " + resultado);
            Console.ReadKey();
            
        }
    }
}
