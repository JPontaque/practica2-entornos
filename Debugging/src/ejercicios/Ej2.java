package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		/*
		 * El error se produce por no limpiar el bufer,
		 * debemos limpiar el bufer siempre que despues de un numero
		 * fueramos a pedir un String.
		 * 
		 * La solucion es limpiar el bufer despues de leer el numero,
		 * y ya estaria arreglado y funcionaria a la perfeccion.
		 */
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo numero");
		}
		
		input.close();
		
		
		
	}
}
