package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JCheckBox;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.TextArea;
import javax.swing.JSlider;
import java.awt.Color;
import javax.swing.JSpinner;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * @author Jose Antonio Pontaque Gavin
 * @since 24/01/2018
 */

public class Ventana extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Jose Pontaque\\Desktop\\workspace\\Eclipse-JoseAntonioPontaqueGavin\\images\\iconoVentanaPrinc_1.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 752, 451);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmHola = new JMenuItem("Abrir");
		mnArchivo.add(mntmHola);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mnArchivo.add(mntmNuevo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JCheckBoxMenuItem chckbxmntmGuardarAutomaticamente = new JCheckBoxMenuItem("Guardar Automaticamente");
		mnArchivo.add(chckbxmntmGuardarAutomaticamente);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar aplicacion");
		mntmCerrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				System.exit(0);
			}
		});
		mnArchivo.add(mntmCerrar);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenu mnFuente = new JMenu("Fuente");
		mnEditar.add(mnFuente);
		
		JMenuItem mntmTipo = new JMenuItem("Tipo");
		mnFuente.add(mntmTipo);
		
		JMenuItem mntmColor = new JMenuItem("Color");
		mnFuente.add(mntmColor);
		
		JMenu mnImagenes = new JMenu("Imagenes");
		mnEditar.add(mnImagenes);
		
		JMenuItem mntmImportar = new JMenuItem("Importar");
		mnImagenes.add(mntmImportar);
		
		JMenu mnTablas = new JMenu("Tablas");
		mnEditar.add(mnTablas);
		
		JMenuItem mntmTamao_1 = new JMenuItem("Tama\u00F1o");
		mnTablas.add(mntmTamao_1);
		
		JMenuItem mntmBorde = new JMenuItem("Borde");
		mnTablas.add(mntmBorde);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(5, 5, 724, 25);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("Abrir archivo");
		btnNewButton.setForeground(Color.GREEN);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Guardar archivo");
		btnNewButton_1.setForeground(Color.GREEN);
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Cerrar archivo");
		btnNewButton_2.setForeground(Color.RED);
		toolBar.add(btnNewButton_2);
		
		TextArea textArea = new TextArea();
		textArea.setBounds(15, 36, 510, 241);
		contentPane.add(textArea);
		
		JSlider slider = new JSlider();
		slider.setFont(new Font("Arial Black", Font.PLAIN, 13));
		slider.setMinorTickSpacing(25);
		slider.setBounds(12, 283, 265, 63);
		slider.setToolTipText("");
		slider.setSnapToTicks(true);
		slider.setForeground(Color.RED);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		contentPane.add(slider);
		
		JLabel lblTamaoFoto = new JLabel("Tama\u00F1o foto");
		lblTamaoFoto.setBounds(108, 349, 78, 16);
		contentPane.add(lblTamaoFoto);
		
		JCheckBox chckbxteGustaLa = new JCheckBox("\u00BFTe gusta la aplicacion?");
		chckbxteGustaLa.setBounds(536, 321, 165, 25);
		contentPane.add(chckbxteGustaLa);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(665, 43, 57, 22);
		contentPane.add(spinner);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		progressBar.setBounds(308, 332, 146, 14);
		contentPane.add(progressBar);
		
		JLabel lblGuardadoAutomatico = new JLabel("Guardado Automatico");
		lblGuardadoAutomatico.setBounds(318, 303, 123, 16);
		contentPane.add(lblGuardadoAutomatico);
		
		JLabel lblContadorDeVisitas = new JLabel("Contador de visitas");
		lblContadorDeVisitas.setBounds(547, 46, 117, 16);
		contentPane.add(lblContadorDeVisitas);
		
		JLabel lbldeseasCerrarEsta = new JLabel("\u00BFDeseas cerrar esta aplicacion?");
		lbldeseasCerrarEsta.setBounds(538, 122, 191, 16);
		contentPane.add(lbldeseasCerrarEsta);
		
		JButton btnSalir = new JButton("Cerrar");
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSalir.setBounds(579, 151, 97, 25);
		contentPane.add(btnSalir);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
