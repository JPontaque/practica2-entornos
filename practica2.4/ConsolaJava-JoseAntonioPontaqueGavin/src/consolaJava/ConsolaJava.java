package consolaJava;

import java.util.Scanner;

public class ConsolaJava {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce una opcion contemplada en el menu: ");
		System.out.println("1 - Contar numeros en una cadena de texto");
		System.out.println("2 - Decir si un numero es primo o no");
		System.out.println("3 - Calcular el numero factorial");
		System.out.println("4 - Salir");

		int opcion;

		opcion = sc.nextInt();
		sc.nextLine();

		switch (opcion) {

		case 1:
			
			encontrarNumeros(sc);

			break;

		case 2:
			
			numeroPrimo(sc);

			break;

		case 3:
			
			numeroFactorial(sc);
			break;

		case 4:
			
			salir(sc);
			
			break;

		default:
			
			otraOpcion(sc);
			break;

		}

		sc.close();

	}

	private static void numeroFactorial(Scanner sc) {
		
		int numero;
		int resultado = 1;
		
		System.out.println("Introduce un numero para hacer su factorial");
		numero = sc.nextInt();
		sc.nextLine();
		
		if (numero != 0){
			
			for(int i = 1; i <= numero; i++){
				
				resultado = resultado * i;
				
			}
			
			System.out.println("El factorial de tu numero '" + numero + "' es = " + resultado);
			
		} else {
			
			System.out.println("El factorial de tu numero '" + numero + "' es = 0");
			
		}
		
		
	}

	private static void otraOpcion(Scanner sc) {
		
		System.out.println("Has introducido una opcion no valida, que tengas buen dia.");
		
	}

	private static void salir(Scanner sc) {
		
		System.out.println("Has seleccionado salir, que tengas buen dia.");
		
	}

	private static void numeroPrimo(Scanner sc) {
		
		int contadorDivisores = 0;
		int numero;
		
		System.out.println("Introduce un numero para comprobar si es primo o no");
		numero = sc.nextInt();
		sc.nextLine();
		
		for(int i = 1; i <= numero; i++){
			
			if(numero % i == 0){
				
				contadorDivisores++;
				
			}
			
		}
		
		if (contadorDivisores > 2){
			
			System.out.println("Tu numero no es primo. Tiene " + contadorDivisores + " divisores.");
			
		} else {
			
			System.out.println("Tu numero es primo. Tiene " + contadorDivisores + " divisores.");
			
		}
		
	}

	private static void encontrarNumeros(Scanner sc) {
		
		int contadorNumeros = 0;
		String cadena;
		
		System.out.println("Introduce una cadena de texto");
		cadena = sc.nextLine();
		
		for(int i = 0; i < cadena.length(); i++){
			
			if (cadena.charAt(i) >= '0' && cadena.charAt(i) <= '9'){
				
				contadorNumeros++;
				
			}
			
		}
		
		System.out.println("Se han encontrado '" + contadorNumeros + "' numeros en tu cadena de texto.");
		
	}

}
