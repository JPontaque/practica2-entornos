﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_JoseAntonioPontaqueGavin
{

    public class Class1
    {

        public static void cadenaReves(String cadena)
        {
            
            String cadenaReves = "";

            cadena = Console.ReadLine();

            for (int i = 0; i < cadena.Length; i++)
            {

                cadenaReves = cadena.ElementAt(i) + cadenaReves;

            }

            Console.WriteLine("Cadena al reves: " + cadenaReves);
            Console.ReadKey();

        }

        public static void cadenaContarNumeros(String cadena)
        {

            int contador = 0;

            for(int i = 0; i < cadena.Length; i++)
            {

                if (cadena.ElementAt(i) >= '0' && cadena.ElementAt(i) <= '9')
                {

                    contador++;

                }

            }

            Console.WriteLine("Tu cadena tiene " + contador + " numeros.");

        }

        public static void cadenaContarVocales(String cadena)
        {

            int contador = 0;

            for(int i = 0; i < cadena.Length; i++)
            {

                if (cadena.ElementAt(i) >= 'a' || cadena.ElementAt(i) <= 'e' || cadena.ElementAt(i) <= 'i' || cadena.ElementAt(i) <= 'o'
                     || cadena.ElementAt(i) <= 'u' || cadena.ElementAt(i) <= 'A' || cadena.ElementAt(i) <= 'E' || cadena.ElementAt(i) <= 'I'
                      || cadena.ElementAt(i) <= 'O' || cadena.ElementAt(i) <= 'U')
                {

                    contador++;

                }

            }

            Console.WriteLine("Tu cadena tiene " + contador + " vocales.");

        }

        public static void cadenaMitad(String cadena)
        {

            Console.WriteLine("La mitad de tu cadena es: " + cadena.Substring(cadena.Length / 2));

        }

        public static void cadenaPorcentajeMayusculas(String cadena)
        {

            int contador = 0;

            for(int i = 0; i < cadena.Length; i++)
            {

                if (cadena.ElementAt(i) >= 'A' && cadena.ElementAt(i) <= 'Z')
                {

                    contador++;

                }

            }

            Console.WriteLine("El porcentaje de mayusculas en tu cadena es: " + (contador*100)/cadena.Length + "%");

        }

    }

}
