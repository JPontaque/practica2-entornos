package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender que realiza este programa
		 */
		
		/*
		 * Lo que el programa hace es saber si un numero es primo o no.
		 * Primero pide un numero, el cual se va a ir dividiendo en un
		 * bucle 'for' desde el 1 hasta por si mismo. Existe un contador
		 * para los divisores, si la cantidad de divisores es mayor a 2, no es primo.
		 * 
		 * Eso es lo que hace este programa.
		 */
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
