package usoLibreriaJava;

import libreriaJava.Mates;

public class UsoLibreriaJava {

	public static void main(String[] args) {
		
		// la clase se llama "Mates".
		
		Mates.numeroFactorial(5);
		Mates.numeroMayor(3, 2);
		Mates.numeroMenor(1, 3);
		Mates.numeroPotencia(2, 3);
		Mates.numeroPrimo(11);
		
		
	}

}
