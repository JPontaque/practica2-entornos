package libreriaJava;

public class Mates {

	public static void numeroMenor(int numero, int numero2) {

		if (numero > numero2) {

			System.out.println(numero2 + " es menor que " + numero);

		} else if (numero < numero2) {

			System.out.println(numero + " es menor que " + numero2);

		} else {

			System.out.println(numero + " es igual que " + numero2);

		}

	}

	public static void numeroMayor(int numero, int numero2) {

		if (numero > numero2) {

			System.out.println(numero + " es mayor que " + numero2);

		} else if (numero < numero2) {

			System.out.println(numero2 + " es mayor que " + numero);

		} else {

			System.out.println(numero + " es igual que " + numero2);

		}

	}

	public static void numeroPotencia(int base, int exponente) {
		
		int resultado = 1;
		
		for(int i = 1; i <= exponente; i++){
			
			resultado = resultado * base;
			
		}
		
		System.out.println("El numero " + base +" elevado a " + exponente + " es = " + resultado);

	}

	public static void numeroFactorial(int numero) {

		int resultado = 1;

		if (numero > 0) {

			for (int i = 1; i <= numero; i++) {

				resultado = resultado * i;

			}

			System.out.println("El factorial de tu numero '" + numero + "' es = " + resultado);

		} else {

			System.out.println("Solo se calcula el factorial de numeros mayores que 0.");

		}

	}

	public static void numeroPrimo(int numero) {

		int contador = 0;

		if (numero >= 0) {

			if (numero > 1) {

				for (int i = 1; i <= numero; i++) {

					if (numero % i == 0) {

						contador++;

					}

				}

				if (contador > 2) {

					System.out.println("Tu numero NO es primo. Tiene " + contador + " divisores.");

				} else {

					System.out.println("Tu numero es primo. Tiene " + contador + " divisores.");

				}

			} else {

				if (numero == 1) {

					System.out.println("Tu numero NO es primo. Tiene 1 divisor.");

				} else {

					System.out.println("Tu numero NO es primo. Tiene 0 divisores.");

				}

			}

		} else {

			System.out.println("Solo se calculan los numeros primos mayores que 0.");

		}

	}

}
